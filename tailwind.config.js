/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        iransans: ['IranSans'],
        cinema: ['cinema'],
        negar: ['negar'],
      },
      // backgroundImage: {
      //   programing: "url('./src/assets/Hand coding-bro (1).svg')",
      //   coding: "url('./src/assets/Code typing-bro.svg')",
      // },
    },
  },
  plugins: [],
};
