import Main from './components/Main';
import './app.css';
export default function App() {
  return (
    <>
      <div className="app">
        <Main />
      </div>
    </>
  );
}
