import movielandpic from './assets/movieland.png';
import weatherpic from './assets/weatherapp.png';
import eshoppic from './assets/eshop.png';
import foodlandpic from './assets/foodland.png';
import portfoliopic from './assets/portfolio.png';

const projects = [
  {
    id: 0,
    title: 'Movieland',
    detail: `         قابلیت جستوجو فیلم و سریال
             
              مشاهده فیلم و سریال های برتر و محبوب
         
              مشاهده جزئیات فیلم و سریال ها
            
              جستوجو بر اساس ژانر فیلم و سریال`,

    tools: [
      'react',
      'axios',
      'react-query',
      'framer-motion',
      'material UI',
      'react-router-dom',
    ],
    linkGithub: 'https://github.com/MhdiRahimi/MovieLand',
    linkLive: 'https://thunderous-shortbread-2da505.netlify.app/',
    image: movielandpic,
  },

  {
    id: 1,
    title: 'eshop',
    detail: `    یک وبسایت جهت خرید لباس های مردانه و زنانه که قابلیت خرید کردن
     لاگین کردن و اکانت ساختن و همچنین جستوجوی لباس است`,

    tools: [
      'react',
      'reduxjs/toolkit',
      'react-slick',
      'framer-motion',
      'chakra UI',
      'react-router-dom',
      'supabase',
    ],
    linkGithub: 'https://github.com/MhdiRahimi/eshop',
    linkLive: 'https://eshop55.netlify.app/',
    image: eshoppic,
  },

  {
    id: 2,
    title: 'foodland',
    detail: `یک وبسایت خرید فست فود که با استفاده از تیلویند استایل دهی شده و شامل چند پیج است `,

    tools: [
      'react',
      'react-router-dom',
      'reduxjs/toolkit',
      'framer-motion',
      'tailwind',
      'swipper',
    ],
    linkGithub: 'https://github.com/MhdiRahimi/foodland',
    linkLive: 'https://foodland50.netlify.app/',
    image: foodlandpic,
  },
  {
    id: 4,
    title: 'Prtfolio',
    detail: `یک وبسایت ساده و شخصی که چندین صفحه دارد و برای ارتباط و رزومه هست`,

    tools: [
      'react',
      'emailjs',
      'framer-motion',
      'tailwind',
      'react-router-dom',
    ],
    linkGithub: 'https://github.com/MhdiRahimi/portfolio/',
    linkLive: 'https://mrahimi-55.netlify.app/',
    image: portfoliopic,
  },

  {
    id: 5,
    title: 'WeatherApp',
    detail: ` یک وب سایت ساده برای دیدن آب و هوا با قابلیت جستو جو کردن شهر و دیدن آب وهوای چند روز بعدی `,

    tools: ['react', 'axios', 'framer-motion', 'tailwind'],
    linkGithub: 'https://github.com/MhdiRahimi/WeatherApp',
    linkLive: 'https://weatherapp-55.netlify.app/',
    image: weatherpic,
  },
];

export default projects;
