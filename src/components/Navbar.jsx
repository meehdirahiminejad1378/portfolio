import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { motion } from 'framer-motion';
export default function Navbar() {
  const [openMenu, setOpenMenu] = useState(false);

  const activeLink = {
    color: '#A6B1E1',
    borderBottom: '3px solid #A6B1E1',
  };
  const activeLinks = {
    color: '#A6B1E1',
  };
  return (
    <>
      <div className=" overflow-x-hidden justify-between md:container md:mx-auto p-2 md:p-6 font-iransans flex ">
        <div className=" text-[#424874]  gap-3  text-md font-black  flex-row-reverse  md:flex hidden">
          <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
            <NavLink
              className={'p-2  '}
              style={({ isActive }) => {
                return isActive ? activeLink : null;
              }}
              to={'/contactme'}
            >
              تماس
            </NavLink>
          </div>
          <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
            <NavLink
              className={'p-2'}
              style={({ isActive }) => {
                return isActive ? activeLink : null;
              }}
              to="/skills"
            >
              مهارت ها
            </NavLink>
          </div>
          <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
            <NavLink
              className={'p-2'}
              style={({ isActive }) => {
                return isActive ? activeLink : null;
              }}
              to="/projects"
            >
              پروژه ها
            </NavLink>
          </div>
          <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
            <NavLink
              className={'p-2'}
              style={({ isActive }) => {
                return isActive ? activeLink : null;
              }}
              to="/aboutme"
            >
              درباره من
            </NavLink>
          </div>
          <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
            <NavLink
              className={'p-2 '}
              style={({ isActive }) => {
                return isActive ? activeLink : null;
              }}
              to="/"
            >
              خانه
            </NavLink>
          </div>
        </div>
        <div
          className={
            openMenu
              ? 'wrapper-menu open  mx-2 my-auto z-50 md:hidden  fixed mt-4 transition-all ease-in-out duration-200 delay-75'
              : 'wrapper-menu   mx-2 my-auto md:hidden   transition-all ease-in-out duration-200 delay-75'
          }
          onClick={() => setOpenMenu(!openMenu)}
        >
          <div className="line-menu half start"></div>
          <div className="line-menu "></div>
          <div className="line-menu  half end"></div>
        </div>

        <motion.div
        onClick={() => setOpenMenu(false)}
          className={
            openMenu
              ? 'sm:w-[50%] w-[97%] right-0 bg-[#dcdcdc] flex justify-center  fixed  h-screen rounded-2xl z-30   md:hidden '
              : 'hidden'
          }
        >
          <div className=" md:hidden my-40 w-[100%] right-0 mx-auto text-[#424874]  top-1/2 content-center text-center gap-3  text-md font-black  grid ">
            <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
              <NavLink
                className={'p-2 '}
                style={({ isActive }) => {
                  return isActive ? activeLinks : null;
                }}
                to="/"
              >
                خانه
              </NavLink>
            </div>
            <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
              <NavLink
                className={'p-2'}
                style={({ isActive }) => {
                  return isActive ? activeLinks : null;
                }}
                to="/skills"
              >
                مهارت ها
              </NavLink>
            </div>
            <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
              <NavLink
                className={'p-2'}
                style={({ isActive }) => {
                  return isActive ? activeLinks : null;
                }}
                to="/projects"
              >
                پروژه ها
              </NavLink>
            </div>
            <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
              <NavLink
                className={'p-2'}
                style={({ isActive }) => {
                  return isActive ? activeLinks : null;
                }}
                to="/aboutme"
              >
                درباره من
              </NavLink>
            </div>
            <div className="hover:text-[#A6B1E1] p-3  transition-all ease-in-out duration-200 delay-75">
              {' '}
              <NavLink
                className={'p-2  '}
                style={({ isActive }) => {
                  return isActive ? activeLinks : null;
                }}
                to={'/contactme'}
              >
                تماس
              </NavLink>
            </div>
          </div>
        </motion.div>
        <div className="text-white md:p-3 p-2 ">
          <NavLink
            to="/"
            className="text-[#A6B1E1] text-3xl font-cinema my-2  "
          >
            مهدی رحیمی نژاد
          </NavLink>
        </div>
      </div>
    </>
  );
}
