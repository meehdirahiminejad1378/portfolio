import React from 'react';
import projects from '../items';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';

export default function ProjectCard() {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.75, ease: 'easeOut', delay: 0 }}
      className="mt-8 md:mt-0 overflow-x-hidden"
    >
      {projects?.map((project) => {
        return (
          <div
            key={project.id}
            className=" flex flex-col px-5 md:px-0  w-full font-iransans mb-16"
          >
            <div className="relative md:w-[80%] w-full flex flex-col-reverse h-full md:flex-row md:space-x-0 space-y-0 md:space-y-0 rounded-xl shadow-lg p-3  mx-auto border border-transparent bg-white">
              <div className="w-full mt-5 md:mt-0 bg-white flex flex-col space-y-2 p-0">
                <h3 className="font-black text-[#424874] md:text-2xl text-xl">
                  {project.title}
                </h3>

                <p className="md:text-md text-[#939cc7] text-base  w-[95%]">
                  {project.detail}
                </p>
                <div className="border-b-[2px] border-[#424874]   w-[95%]"></div>
                <p className="text-xl font-black text-[#424874]  grid">
                  ابزار های استفاده شده
                </p>
                <div className="flex flex-wrap w-[95%]">
                  {project.tools.map((tool, index) => {
                    return (
                      <>
                        <span
                          key={index}
                          className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]"
                        >
                          <span className="text-center">{tool}</span>
                        </span>
                      </>
                    );
                  })}
                </div>
                <div className="flex sm:justify-start justify-between gap-8 px-3 font-thin text-sm py-2">
                  <button
                    onClick={() =>
                      window.open(project.linkGithub, '_blank', 'noreferrer')
                    }
                    className=" px-5 py-2 shadow-lg font-iransans bg-[#7478A8] hover:bg-[#A6B1E1] text-[#f1f6f9]  rounded-lg   transition-all ease-in-out duration-200 delay-75"
                  >
                    مشاهده کد
                  </button>
                  <button
                    onClick={() =>
                      window.open(project.linkLive, '_blank', 'noreferrer')
                    }
                    className=" px-4 py-2  shadow-lg font-iransans bg-[#7478A8] hover:bg-[#A6B1E1] text-[#f1f6f9]  rounded-lg transition-all ease-in-out duration-200 delay-75"
                  >
                    مشاهده سایت
                  </button>
                </div>
              </div>

              <div className="w-full md:w-[50%] h-[100%] bg-transparent grid place-items-center mx-auto">
                <img
                  src={project.image}
                  alt={project.title}
                  className="rounded-xl h-[100%]  bg-auto  bg-no-repeat bg-center"
                />
              </div>
            </div>
          </div>
        );
      })}
    </motion.div>
  );
}

// eslint-disable-next-line no-lone-blocks
{
  /*
     <span className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span className="text-center">react</span>
                  </span>
                  <span className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span className="text-center">react-query</span>
                  </span>
                  <span className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span className="text-center">axios</span>
                  </span>
                  <span className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span className="text-center">framer-motion</span>
                  </span>
                  <span className=" text-center justify-center transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span className="text-center">material UI</span>
                  </span>
 */
}
