import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

import Contact from '../pages/Contact';
import Project from '../pages/Project';
import Skills from '../pages/Skills';
import Home from '../pages/Home';
import About from '../pages/About';
import { AnimatePresence } from 'framer-motion';
import Navbar from './Navbar';

export default function Main() {
  let location = useLocation();

  return (
    <AnimatePresence>
      <Navbar />
      <Routes location={location} key={location.pathname}>
        <Route path="/" element={<Home />} />
        <Route path="aboutme" element={<About />} />
        <Route path="skills" element={<Skills />} />
        <Route path="projects" element={<Project />} />
        <Route path="contactme" element={<Contact />} />
      </Routes>
    </AnimatePresence>
  );
}
