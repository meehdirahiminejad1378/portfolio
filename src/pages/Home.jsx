import React, { useEffect, useRef } from 'react';
import { Location, Call, Whatsapp } from 'iconsax-react';
import { FaTelegram, FaWhatsapp, FaGithub } from 'react-icons/fa';
import { FiMail } from 'react-icons/fi';
import { BiLogoTelegram } from 'react-icons/bi';
import Typed from 'typed.js';
import { motion } from 'framer-motion';
import { NavLink } from 'react-router-dom';

export default function Home() {
  const el = useRef(null);

  useEffect(() => {
    const typed = new Typed(el.current, {
      strings: ['Hello, World'], // Strings to display
      // Speed settings, try diffrent values untill you get good results
      startDelay: 600,
      typeSpeed: 100,
      backSpeed: 100,
      backDelay: 300,
      loop: true,
    });

    // Destropying
    return () => {
      typed.destroy();
    };
  }, []);
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.55, ease: 'easeOut', delay: 0 }}
    >
      <motion.div
        initial={{ y: 100 }}
        animate={{ y: 0 }}
        transition={{ duration: 0.55, ease: 'easeOut', delay: 0 }}
        className="md:flex  justify-center w-full md:container mx-auto md:px-6 px-4 md:mt-12 mt-[9rem] my-auto"
      >
        <div className="grid gap-y-4 w-full md:mt-8   md:px-[8rem] justify-self-center text-center md:mb-8">
          <h1 className="md:text-6xl text-3xl  text-[#424874]  font-cinema my-auto text-center ">
            مهدی رحیمی نژاد
          </h1>
          <h5 className="text-[#424874] md:text-xl text-sm font-iransans mt-0">
            برنامه نویس و توسعه دهنده وب سایت سمت کاربر
          </h5>

          <div className="flex gap-8 justify-center ">
            <div className="flex gap-2 ">
              <span>
                <Location size="20" color="#A6B1E1" />
              </span>
              <h3 className="text-[#424874] text-sm md:text-lg font-iransans ">
                شیراز
              </h3>
            </div>
            <div className="flex gap-2 ">
              <span>
                <Call size="20" color="#A6B1E1" />
              </span>
              <h3 className="text-[#424874] cursor-pointer font-negar md:text-lg text-sm">
                09055491575
              </h3>
            </div>
          </div>
        </div>
      </motion.div>

      <div className="text-[#424874]  flex md:grid md:container md:grid-cols-2 md:mx-auto md:p-6   gap-4 ">
        <div className=" md:col-span-1 justify-start hidden md:grid md:container  md:mx-auto md:px-6  gap-4 ">
          <span className=" group cursor-pointer relative inline-block  text-center w-6   transition-all ease-in-out duration-200 delay-75 ">
            <i
              onClick={() =>
                window.open(
                  'https://t.me/mhdirahiminejad',
                  '_blank',
                  'noreferrer'
                )
              }
              className="cursor-pointer w-6 hover:text-[#A6B1E1]   transition-all ease-in-out duration-200 delay-75 "
            >
              <BiLogoTelegram size={'24px'} />
            </i>
            <p className="opacity-0 w-52 text-[#424874] text-start px-4 rounded-lg mx-12 absolute z-10 group-hover:opacity-100 bottom-[3px] font-normal text-sm   transition-all ease-in-out duration-200 delay-75  pointer-events-none">
              Telegram
            </p>
          </span>
          <span className=" group cursor-pointer relative inline-block  text-center w-6   transition-all ease-in-out duration-200 delay-75 ">
            <i
              onClick={() =>
                window.open(
                  'https://wa.me/qr/KBW3FQRAVRE5N1',
                  '_blank',
                  'noreferrer'
                )
              }
              className="cursor-pointer w-6 hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75"
            >
              <ion-icon name="logo-whatsapp"></ion-icon>
            </i>
            <p className="opacity-0 w-52  text-[#424874] text-start px-4 rounded-lg mx-12 absolute z-10 group-hover:opacity-100 bottom-[3px] font-normal text-sm   transition-all ease-in-out duration-200 delay-75  pointer-events-none">
              Whatsapp
            </p>
          </span>
          <span className=" group cursor-pointer relative inline-block  text-center w-6   transition-all ease-in-out duration-200 delay-75 ">
            <i
              onClick={() =>
                window.open(
                  'https://github.com/MhdiRahimi',
                  '_blank',
                  'noreferrer'
                )
              }
              className="cursor-pointer w-6 hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75 "
            >
              <ion-icon name="logo-github"></ion-icon>
            </i>
            <p className="opacity-0 w-52  text-[#424874] text-start px-4 rounded-lg mx-12 absolute z-10 group-hover:opacity-100 bottom-[3px] font-normal text-sm   transition-all ease-in-out duration-200 delay-75  pointer-events-none">
              Github
            </p>
          </span>
          <span className=" group cursor-pointer relative inline-block  text-center w-6   transition-all ease-in-out duration-200 delay-75">
            <NavLink to='/contactme'
             
              className="  cursor-pointer hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75"
            >
              <ion-icon name="mail-outline"></ion-icon>
            </NavLink>
            <p className="opacity-0 w-52  text-[#424874] text-start px-4 rounded-lg mx-12 absolute z-10 group-hover:opacity-100 bottom-[3px] font-normal text-sm   transition-all ease-in-out duration-200 delay-75  pointer-events-none">
              meehdirahiminejad1378@gmail.com
            </p>
          </span>
        </div>
        <div
          className=" justify-center md:justify-start flex mt-10 align-middle md:px-6 w-full md:mt-auto mb-2 text-[#424874] text-2xl "
          style={{ direction: 'ltr' }}
        >
          <span ref={el}></span>
        </div>
      </div>
    </motion.div>
  );
}
