import React from 'react';
import skillPhoto from '../assets/Code typing-bro.png';
import { motion } from 'framer-motion';
export default function Skills() {
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.75, ease: 'easeOut', delay: 0 }}
        className="flex flex-wrap  justify-center w-full md:container md:mx-auto  mt-2 my-auto "
      >
        <div className="w-full mx-auto mt-8 md:mt-0">
          <h1 className="font-cinema text-3xl text-center text-[#424874]">
            مهارت ها
          </h1>
          <div className="md:px-8 mx-auto py-4 flex justify-center text-center ">
            <div className="relative     rounded">
              <div className="absolute top-0 h-2 md:w-[20vw] sm:w-[50vw] w-[70vw]  rounded shim-green"></div>
            </div>
          </div>
        </div>
        <div
       
          className=" md:w-[40%] md:hidden w-full m-0 md:ml-8 "
        >
          <img
            className=" bg-auto bg-no-repeat bg-center  "
            src={skillPhoto}
            alt="img"
          />
        </div>
        <motion.div
          initial={{ x: 100, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ duration: 0.75, ease: 'easeOut', delay: 0.75 }}
          className="md:flex flex-wrap md:justify-between justify-center "
        >
          <div className="md:w-[50%] pb-6 md:pb-0 w-full md:mr-8 m-0">
            <div className=" mt-16">
              <div className="flex-wrap flex gap-x-6  gap-y-4 justify-items-center ">
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <ion-icon name="logo-html5"></ion-icon>
                    <span class="mr-1">Html</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <ion-icon name="logo-css3"></ion-icon>
                    <span class="mr-1">CSS</span>
                  </span>
                </div>

                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <ion-icon name="logo-react"></ion-icon>
                    <span class="mr-1">React</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <ion-icon name="logo-javascript"></ion-icon>
                    <span class="mr-1">Javascript</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <ion-icon name="git-branch-outline"></ion-icon>
                    <span class="mr-1">Git</span>
                  </span>
                </div>
              </div>
              <div className=" md:w-[100%] w-[80%]  mx-auto md:mx-0 border-b-[5px] text-[#212A3E] mt-10 rounded-md"></div>
              <div className="flex flex-wrap gap-x-4 gap-y-4 mt-5 cursor-pointer">
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Material UI</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Chakra UI</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Tailwind</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Bootstrap</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">REST API</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Risponsive web</span>
                  </span>
                </div>
                <div className="cursor-pointer">
                  <span class=" hover:scale-110 transition-all ease-in-out duration-200 delay-75 drop-shadow-lg inline-flex items-center m-2 px-4 py-2 bg-[#DCD6F7] hover:bg-[#A6B1E1] rounded-full text-sm font-semibold text-[#212A3E]">
                    <span class="mr-1">Redux/toolkit</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className=" md:w-[40%] w-full m-0 md:ml-8 hidden md:flex">
            <motion.img
              initial={{ x: -100 }}
              animate={{ x: 0 }}
              transition={{ duration: 0.75, ease: 'easeOut', delay: 0.75 }}
              className=" bg-auto bg-no-repeat bg-center  "
              src={skillPhoto}
              alt="img"
            />
          </div>
        </motion.div>
      </motion.div>
    </>
  );
}
