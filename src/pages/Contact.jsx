/* eslint-disable no-lone-blocks */
import React, { useRef } from 'react';
import { BiLogoTelegram } from 'react-icons/bi';
import { motion } from 'framer-motion';
import emailjs from '@emailjs/browser';

export default function Contact() {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        'service_oznr72q',
        'template_9mk7bsi',
        form.current,
        'c_vIm25ymV04PTAY0'
      )
      .then(
        (result) => {
          alert('Your mail is sent!');
        },
        (error) => {
          alert('Failed!');
        }
      );
    e.target.reset();
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.75, ease: 'easeOut', delay: 0 }}
      className="flex flex-wrap w-full md:container md:mx-auto  mt-2 my-auto "
    >
      <div className="w-full mx-auto  mt-8 md:mt-0">
        <h1 className="font-cinema text-3xl text-center text-[#424874]">
          تماس با من
        </h1>
        <div className="md:px-8 mx-auto py-4 flex justify-center text-center ">
          <div className="relative     rounded">
            <div className="absolute top-0 h-2 md:w-[20vw] sm:w-[50vw] w-[70vw]  rounded shim-green"></div>
          </div>
        </div>
      </div>

      <div className="flex md:grid justify-center md:grid-cols-12 flex-col-reverse w-full md:px-10 p-4 mt-10 ">
        <div className="  text-left px-4 md:col-span-4 grid md:border-l-[5px] border-[#A6B1E1]    gap-y-[2rem] mb-6   justify-end">
          <div className="text-end md:grid   justify-end   flex font-iransans">
            <div className=" flex-wrap md:m-0 ml-4">
              <h2 className="text-[#A6B1E1]"> نام</h2>
              <h2 className="text-[#424874]">مهدی رحیمی نژاد</h2>
            </div>
            <div className="my-auto">
              <span className="text-[#424874] md:hidden group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
                <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
                  <ion-icon name="logo-ionic"></ion-icon>
                </i>
              </span>
            </div>
          </div>
          <div className="text-end  justify-end   flex  font-iransans">
            <div className=" flex-wrap md:m-0 ml-4">
              <h2 className="text-[#A6B1E1]">آدرس</h2>
              <h2 className="text-[#424874]">شیراز</h2>
            </div>
            <div className="my-auto">
              <span className="text-[#424874] md:hidden group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
                <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
                  <ion-icon name="map-outline"></ion-icon>
                </i>
              </span>
            </div>
          </div>{' '}
          <div className="text-end   justify-end   flex font-iransans">
            <div className=" flex-wrap md:m-0 ml-4">
              <h2 className="text-[#A6B1E1]">تماس</h2>
              <h2 className="text-[#424874]">09055491575</h2>
            </div>
            <div className="my-auto">
              <span className="text-[#424874] md:hidden group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
                <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
                  <ion-icon name="call-outline"></ion-icon>
                </i>
              </span>
            </div>
          </div>{' '}
          <div className="text-end  justify-end   flex  font-iransans">
            <div className=" flex-wrap md:m-0 ml-4">
              <h2 className="text-[#A6B1E1]">آدرس الکترونیک</h2>
              <h2 className="text-[#424874] text-sm sm:text-base">
                meehdirahiminejad1378@gmail.com
              </h2>
            </div>
            <div className="my-auto">
              <span className="text-[#424874] md:hidden group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
                <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
                  <ion-icon name="send-outline"></ion-icon>
                </i>
              </span>
            </div>
          </div>
        </div>
        <div className="text-[#424874] md:grid hidden  gap-y-[2rem] mb-6   text-3xl px-8">
          <span className=" group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
              <ion-icon name="logo-ionic"></ion-icon>
            </i>
          </span>

          <span className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75">
              <ion-icon name="map-outline"></ion-icon>
            </i>
          </span>
          <span className=" group cursor-pointer relative  w-6    transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75 ">
              <ion-icon name="call-outline"></ion-icon>
            </i>
          </span>
          <span className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75">
            <i className="  cursor-pointer hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75">
              <ion-icon name="send-outline"></ion-icon>
            </i>
          </span>
        </div>
        <div className=" md:pb-0 pb-6 w-[100%] mt-1 md:col-span-7 grid md:mx-4 content-center my-auto">
          <form
            ref={form}
            onSubmit={sendEmail}
            className="space-y-6 placeholder-slate-950 w-[100%]"
          >
            <div className="md:flex md:space-y-0 space-y-6 justify-between ">
              <input
                name="user_email"
                id="name"
                type="text"
                placeholder="نام"
                className="md:w-[48%] w-full placeholder-slate-950  p-3 rounded  bg-[#A6B1E1] focus:outline-none focus:border-[#424874] focus:border-b-2"
              />

              <input
                name="user_subject"
                id="name"
                type="text"
                placeholder=" موضوع"
                className="md:w-[48%] w-full  placeholder-slate-950  p-3 rounded  bg-[#A6B1E1] focus:outline-none focus:border-[#424874] focus:border-b-2"
              />
            </div>
            <div className="w-[100%]">
              <input
                name="user_email"
                id="email"
                type="email"
                placeholder="پست الکترونیک"
                className="w-full p-3 rounded placeholder-slate-950  bg-[#A6B1E1] focus:outline-none focus:border-[#424874] focus:border-b-2"
              />
            </div>
            <div className="w-[100%]">
              <textarea
                name="message"
                id="message"
                rows="3"
                placeholder="پیام"
                className="w-full p-3 rounded placeholder-slate-950  bg-[#A6B1E1] focus:outline-none focus:border-[#424874] focus:border-b-2"
              ></textarea>
              <button className=" mt-4 md:mb-0 mb-4  mr-auto flex px-8 py-2  font-iransans bg-[#424874] hover:bg-[#7478A8] text-[#f1f6f9]   rounded-full  transition-all ease-in-out duration-200 delay-75">
                ارسال پیام
              </button>
            </div>
          </form>
        </div>
      </div>
    </motion.div>
  );
}

{
  /**
   * 
   * 
  <div className="flex justify-between">
              <h2 className="text-[A6B1E1]">نام</h2>
              <h2 className="text-[#424874]">مهدی رحیمی نژاد</h2>
              <div className='border-r-2'>
                <span className=" group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
                  <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
                    <ion-icon name="logo-ionic"></ion-icon>
                  </i>
                </span>
              </div>
            </div>


   <div className="  text-left px-4 col-span-4 border-l-[5px] border-[#A6B1E1]   grid  gap-y-[2rem] mb-6   justify-end">
          <div className="text-end  font-iransans">
            <h2 className="text-[A6B1E1]">نام</h2>
            <h2 className="text-[#424874]">مهدی رحیمی نژاد</h2>
          </div>
          <div className="text-end  font-iransans">
            <h2 className="text-[A6B1E1]">آدرس</h2>
            <h2 className="text-[#424874]">شیراز</h2>
          </div>{' '}
          <div className="text-end  font-iransans">
            <h2 className="text-[A6B1E1]">تماس</h2>
            <h2 className="text-[#424874]">09055491575</h2>
          </div>{' '}
          <div className="text-end  font-iransans">
            <h2 className="text-[A6B1E1]">آدرس الکترونیک</h2>
            <h2 className="text-[#424874]">meehdirahiminejad1378@gmail.com</h2>
          </div>
        </div>
        <div className="text-[#424874] grid   gap-y-[2rem] mb-6   text-3xl px-8">
          <span className=" group cursor-pointer relative flex  w-6  transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]   transition-all ease-in-out duration-200 delay-75 ">
              <ion-icon name="logo-ionic"></ion-icon>
            </i>
          </span>

          <span className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75">
              <ion-icon name="map-outline"></ion-icon>
            </i>
          </span>
          <span className=" group cursor-pointer relative  w-6    transition-all ease-in-out duration-200 delay-75 ">
            <i className="cursor-pointer w-6 hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75 ">
              <ion-icon name="call-outline"></ion-icon>
            </i>
          </span>
          <span className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75">
            <i className="  cursor-pointer hover:text-[#7478A8]  transition-all ease-in-out duration-200 delay-75">
              <ion-icon name="send-outline"></ion-icon>
            </i>
          </span>
        </div>
 */
}
