import React from 'react';
import aboutPhoto from '../assets/Programming-rafiki.png';
import { NavLink } from 'react-router-dom';
import { BiLogoTelegram } from 'react-icons/bi';
import { motion } from 'framer-motion';
import resume from '../assets/mhdiRahiminejad.pdf';
export default function About() {
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.55, ease: 'easeOut', delay: 0 }}
        className="flex flex-wrap justify-center w-full md:container md:mx-auto overflow-x-hidden md:mt-2 mt-6 my-auto "
      >
        <div className="w-full mx-auto  mt-8 md:mt-0">
          <h1 className="font-cinema text-3xl text-center text-[#424874]">
            درباره من
          </h1>
          <div className="md:px-8 mx-auto py-4 flex justify-center text-center ">
            <div className="relative     rounded">
              <div className="absolute top-0 h-2 md:w-[20vw] sm:w-[50vw] w-[70vw]  rounded shim-green"></div>
            </div>
          </div>
        </div>
        <motion.div
          initial={{ x: -100, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ duration: 0.75, ease: 'easeOut', delay: 0.75 }}
          className="md:w-[50%] w-[80%] mx-auto md:hidden flex "
        >
          <img
            className=" bg-auto bg-no-repeat bg-center  "
            src={aboutPhoto}
            alt="img"
          />
        </motion.div>
        <div className="md:flex grid justify-between mx-auto ">
          <motion.div
            initial={{ x: 100, opacity: 0 }}
            animate={{ x: 0, opacity: 1 }}
            transition={{ duration: 0.75, ease: 'easeOut', delay: 0.75 }}
            className="md:w-[60%] w-full"
          >
            <div className="lg:mx-20 mt-10 px-4 md:px-4">
              <h1 className="text-4xl font-cinema  my-auto text-start text-[#424874] ">
                مهدی رحیمی نژاد
              </h1>
              <h2 className="text-[#7478A8] sm:text-lg text-sm  font-iransans mt-4 justify-start text-justify">
                من توسعه دهنده وب هستم که عاشق طراحی وب سایت های زیبا و
                کاربرپسند هستم. من به دنبال چالش های جدید در زمینه توسعه وب سایت
                ها هستم و به دنبال راه حل های نوآورانه برای مشکلات مختلف در این
                حوزه می گردم.
              </h2>
              <div className="w-[100%] border-b-[5px] text-[#212A3E] mt-10 rounded-md"></div>
            </div>
            <div className="md:flex grid lg:mx-20  px-4  mt-10 justify-between font-iransans text-[#424874] ">
              <div className="flex gap-2">
                <p className="">نام : </p>
                <p className="">مهدی رحیمی نژاد</p>
              </div>
              <div className="flex gap-2">
                <p>سن : </p>
                <p className="font-negar">23</p>
              </div>
            </div>
            <div className=" md:flex grid lg:mx-20  px-4    md:mt-10 mt-0 justify-between font-iransans text-[#424874]">
              <div className="flex-wrap sm:flex gap-2">
                <p>ایمیل : </p>
                <NavLink className="hover:text-[#A6B1E1]">
                  {' '}
                  meehdirahiminejad@gmail.com
                </NavLink>
              </div>

              <div className="flex gap-2">
                <p>آدرس : </p>
                <p>شیراز</p>
              </div>
            </div>

            <div className="md:flex grid lg:mx-20  px-4   justify-start mt-10 md:justify-between">
              <a href={resume} download target="_blank" rel="noreferrer">
                <button className=" p-2  font-iransans bg-[#424874] hover:bg-[#A6B1E1] text-[#f1f6f9]   rounded-full  transition-all ease-in-out duration-200 delay-75">
                  دانلود رزومه
                </button>
              </a>

              <div className="text-[#424874] flex md:gap-4 gap-6 my-auto md:mt-0 mt-4 md:justify-end">
                <span
                  onClick={() =>
                    window.open(
                      'https://t.me/mhdirahiminejad',
                      '_blank',
                      'noreferrer'
                    )
                  }
                  className=" group cursor-pointer relative  w-6   transition-all ease-in-out duration-200 delay-75 "
                >
                  <i className="cursor-pointer w-6 hover:text-[#A6B1E1]   transition-all ease-in-out duration-200 delay-75 ">
                    <BiLogoTelegram size={'24px'} />
                  </i>
                </span>
                <span
                  onClick={() =>
                    window.open(
                      'https://wa.me/qr/KBW3FQRAVRE5N1',
                      '_blank',
                      'noreferrer'
                    )
                  }
                  className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75 "
                >
                  <i className="cursor-pointer w-6 hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75">
                    <ion-icon name="logo-whatsapp"></ion-icon>
                  </i>
                </span>
                <span
                  onClick={() =>
                    window.open(
                      'https://github.com/MhdiRahimi',
                      '_blank',
                      'noreferrer'
                    )
                  }
                  className=" group cursor-pointer relative  w-6   transition-all ease-in-out duration-200 delay-75 "
                >
                  <i className="cursor-pointer w-6 hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75 ">
                    <ion-icon name="logo-github"></ion-icon>
                  </i>
                </span>
                <span
                  onClick={() => alert('meehdirahiminejad1378@gmail.com')}
                  className=" group cursor-pointer relative   w-6   transition-all ease-in-out duration-200 delay-75"
                >
                  <i className="  cursor-pointer hover:text-[#A6B1E1]  transition-all ease-in-out duration-200 delay-75">
                    <ion-icon name="mail-outline"></ion-icon>
                  </i>
                </span>
              </div>
            </div>
          </motion.div>
          <motion.div
            initial={{ x: -100, opacity: 0 }}
            animate={{ x: 0, opacity: 1 }}
            transition={{ duration: 0.75, ease: 'easeOut', delay: 0.75 }}
            className="md:w-[50%] w-[80%] mx-auto md:flex hidden "
          >
            <img
              className=" bg-auto bg-no-repeat bg-center  "
              src={aboutPhoto}
              alt="img"
            />
          </motion.div>
        </div>
      </motion.div>
    </>
  );
}
