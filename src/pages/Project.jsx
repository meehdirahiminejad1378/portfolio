import React from 'react';
import ProjectCard from '../components/ProjectCard';

export default function Project() {
  return (
    <>
      <div className="flex flex-wrap justify-center w-full md:container md:mx-auto md:p-6 mt-2 my-auto">
        <div className="w-full mx-auto mt-8 md:mt-0">
          <h1 className="font-cinema text-3xl text-center text-[#424874]">
            پروژه ها
          </h1>
          <div className="md:px-8 mx-auto py-4 flex justify-center text-center ">
            <div className="relative     rounded">
              <div className="absolute top-0 h-2 md:w-[20vw] sm:w-[50vw] w-[70vw]  rounded shim-green"></div>
            </div>
          </div>
        </div>

        <ProjectCard />
      </div>
    </>
  );
}
